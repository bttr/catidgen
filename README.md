# catidgen

FlatPress Category ID Generator / Checker written in Turbo Pascal for DOS in 2022

## Sample Output

In this example category names are in German. CATIDGEN created ID 163 for the new category "Neue Kategorie". No duplicate IDs were found.

```
Reading categories list...
Generating new list...

Allgemein :272
Software :668
--DOS :810
--Linux :718
--Windows :444
--Android :560
--Web :639
----FlatPress :826
Programmieren :924
--Assembler :941
--C :363
--Pascal :359
----Neue Kategorie :163
--PHP :488

13 old ID(s) found
1 new ID(s) generated

Please, insert new list to FlatPress.
```
